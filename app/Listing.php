<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Malhal\Geographical\Geographical;

class Listing extends Model
{

	use Geographical;
	protected static $kilometers = true;
    

	protected $casts = [
        'latitude' 	=> 'double',
        'longitude' => 'double',
    ];

    protected $fillable = [
        'list_name', 'address', 'latitude', 'longitude', 'submitter_id'
    ];

    public function user () {
    	return $this->belongsTo(User::class, 'submitter_id');
    }

}
