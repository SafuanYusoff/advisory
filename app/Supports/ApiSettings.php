<?php

namespace App\Supports;

use App\Http\Resources\EmptyResource;
use App\Models\AppVersion;
use Dingo\Api\Http\Request;

trait ApiSettings
{

    /**
     * @var array
     */
    public $data = [];

    /**
     * @var array
     */
    public $error_message = [];


    /**
     * @param $request
     */
    public function with($request)
    {
        return [
            'status' => $this->status,
        ];
    }


    /**
     * @return mixed
     */
    public function prepareStatusData($status = array())
    {
        $data = collect([
            'code'                           => isset($status['code']) ? $status['code'] : '200',
            'message'                              => isset($status['message']) ? $status['message'] : '',
        ]);

        return $data;
    }

 
    /**
     * @param $error_message
     * @param $error_code
     */
    public function failedAppData($error_message, $error_code = 999999)
    {
        return [
            'code'       => $error_code,
            'message'    => $error_message,
        ];
    }


    /**
     * @param $request
     * @param $message
     * @param $error_code
     */
    public function failedResponse($request, $message, $error_code = 99999)
    {
        $status = $this->failedAppData($message, $error_code);

        $emptyData = collect();
        $emptyData->status = $this->prepareStatusData($status);

        return new EmptyResource($emptyData);
    }

}
