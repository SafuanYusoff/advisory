<?php

namespace App\Supports;


trait TraitHelpers {

    public function convertNullToString($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = is_null($value) ? '' : $value;

            }
        } else {
            $data = is_null($data) ? '' : $data;
        }

        $new_data = $data;

        return $new_data;
    }

}
