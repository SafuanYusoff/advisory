<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type', 'u')->paginate(5);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        ##VALIDATION PART##
        $rules = [
          'name'            => 'required',
          'email'           => 'required|email|unique:users,email',
        ];

        $message = [
            'name.required'       => "Name is required.",
            'email.email'         => "Email is required.",
        ];

        $this->validate($request, $rules, $message);
        ##VALIDATION PART##

        ##STORE IN DB
        User::create([
            'name'      => $request->input('name'),
            'email'     => $request->email,
            'type'      => 'u',
            'password'  => bcrypt('password'),
        ]);

        ###
        return redirect()->route('user.index')->with(['success-msg' => 'Record successfully created']);
        ###
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($user = User::find($id)) {

            return view('users.view', compact('user'));
        }

        return redirect()->route('user.index')->with(['error-msg' => 'Record not found.']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($user = User::find($id)) {

            return view('users.edit', compact('user'));
        }

        return redirect()->route('user.index')->with(['error-msg' => 'Record not found.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        ##VALIDATION PART##
        $rules = [
        'name'            => 'required',
      ];

      $message = [
          'name.required'       => "name is required",
      ];

      $this->validate($request, $rules, $message);
      ##VALIDATION PART##

      if (!$user = User::find($id)) {
          return redirect()->route('user.index')->with(['error-msg' => 'Record not found.']);
      }


      ###
      $user->name = $request->input('name');
      $user->save();
      ###
      // $user->update([
      //     'name'  => $request->input('name'),
      // ]);


      return redirect()->route('user.index')->with(['success-msg' => 'Record successfully updated.']);

  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        if ($user = User::find($id)) {

            if($user->type == 'a'){
                return redirect()->route('user.index')->with(['error-msg' => 'Record not found.']);
            }

            $user->delete();

            return redirect()->route('user.index')->with(['success-msg' => 'Record successfully deleted.']);

        }

        return redirect()->route('user.index')->with(['error-msg' => 'Record not found.']);

    }
}
