<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listings = Listing::with('user')->paginate(5);

        return view('listing.index', compact('listings'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('listing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ##VALIDATION START##
        $rules = [
          'name'            => 'required',
          'address'         => 'required',
          'latitude'        => 'required|numeric',
          'longitude'       => 'required|numeric',
        ];

        $message = [];

        $this->validate($request, $rules, $message);
        ##VALIDATION END##

        ##STORE IN DB
        Listing::create([
            'list_name' => $request->input('name'),
            'address'   => $request->input('address'),
            'latitude'  => $request->input('latitude'),
            'longitude' => $request->input('longitude'),
            'submitter_id' => \Auth::user()->id,
        ]);

        ###
        return redirect()->route('listing.index')->with(['success-msg' => 'Record successfully created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($listing = Listing::find($id)) {

            return view('listing.view', compact('listing'));
        }

        return redirect()->route('listing.index')->with(['error-msg' => 'Record not found.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($listing = Listing::find($id)) {

            return view('listing.edit', compact('listing'));
        }

        return redirect()->route('listing.index')->with(['error-msg' => 'Record not found.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ##VALIDATION PART##
        $rules = [
          'name'            => 'required',
          'address'         => 'required',
          'latitude'        => 'required|numeric',
          'longitude'       => 'required|numeric',
        ];

        $message = [];

        $this->validate($request, $rules, $message);
        ##VALIDATION PART##

        if (!$listing = Listing::find($id)) {
            return redirect()->route('listing.index')->with(['error-msg' => 'Record not found.']);
        }

        ##STORE IN DB
        $listing->update([
            'list_name' => $request->input('name'),
            'address'   => $request->input('address'),
            'latitude'  => $request->input('latitude'),
            'longitude' => $request->input('longitude'),
        ]);

        ###
        return redirect()->route('listing.index')->with(['success-msg' => 'Record successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($listing = Listing::find($id)) {

            $listing->delete();

            return redirect()->route('listing.index')->with(['success-msg' => 'Record successfully deleted.']);

        }

        return redirect()->route('listing.index')->with(['error-msg' => 'Record not found.']);
    }
}
