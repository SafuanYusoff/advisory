<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ListingResource;
use App\Listing;
use App\Supports\ApiSettings;
use Illuminate\Http\Request;
use Validator;

class ListingController extends Controller
{
	use ApiSettings;

    public function listing(Request $request)
    {
        $user = $request->user();
    	

    	##VALIDATION PART##
        $rules = [
          'latitude'           => 'required',
          'longitude'          => 'required',
        ];

        $message = [];


        ##VALIDATION PART##    	
        $validator = Validator::make($request->input(), $rules);


        if ($validator->fails()) {
            return $this->failedResponse($request, $validator->errors()->first());
        }


    	$listings = Listing::distance($request->input('latitude'), $request->input('longitude'))->get();

    	return ListingResource::collection($listings)->additional(['status' => $this->prepareStatusData(['message' => 'Listing successfully retrieved.'])]);

    }
}
