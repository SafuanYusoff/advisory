<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Supports\ApiSettings;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class AuthControlller extends Controller
{

    use ApiSettings;

    public function login(Request $request)
    {    

        ##VALIDATION PART##
        $rules = [
          'email'              => 'required|email',
          'password'           => 'required',
        ];

        $message = [
            'name.required'       => "Name is required.",
            'email.email'         => "Email is required.",
        ];

        ##VALIDATION PART##    	

        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return $this->failedResponse($request, $validator->errors()->first());
        }

    	if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])){ 
    		
    		$user = Auth::user();

            ##ALLOW USER TYPE == U ONLY CAN ACCESS
            if($user->type != 'u'){
                return $this->failedResponse($request, 'Only user can access this app.');
            }

            ##Gen token
    		$token = $user->createToken('Personal Access Token')->accessToken;
    		$user->token = $token;
            $user->status = $this->prepareStatusData(['message' => 'Access Granted']);

            return new UserResource($user);

    	}

        return $this->failedResponse($request, 'invalid-credential');

    }

    public function me(Request $request)
    {

        $user = $request->user();
        $user->token = null;
        $user->status = $this->prepareStatusData(['message' => 'Access Granted']);
        return new UserResource($user);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
