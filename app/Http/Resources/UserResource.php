<?php

namespace App\Http\Resources;

use App\Supports\ApiSettings;
use App\Supports\TraitHelpers;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    use ApiSettings, TraitHelpers;

    /**
     * @var string
     */
    public static $wrap = 'data';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = [
            'id'               => $this->id,
            'email'            => $this->email,
            'name'             => $this->name,
        ];
        
        isset($this->token) ? $user['token'] = $this->token : '';
        
        return $this->convertNullToString($user);
    }
}
