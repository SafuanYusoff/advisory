<?php

namespace App\Http\Resources;

use App\Supports\ApiSettings;
use App\Supports\TraitHelpers;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingResource extends JsonResource
{

    use ApiSettings, TraitHelpers;

    /**
     * @var string
     */
    public static $wrap = 'data';
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $listing = [
            'id'               => $this->id,
            'list_name'        => $this->list_name,
            // 'address'          => $this->address,
            'distance'         => number_format($this->distance, 3),
        ];

        return $this->convertNullToString($listing);
    }
}
