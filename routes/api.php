<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->group(function () {

	## LOGIN ROUTE
	Route::post('login', 'AuthControlller@login');

	Route::post('listing', 'ListingController@listing')->middleware('auth:api');
	Route::get('me', 'AuthControlller@me')->middleware('auth:api');
	Route::get('logout', 'AuthControlller@logout')->middleware('auth:api');

});