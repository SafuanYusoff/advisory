@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Listing Table</h2>
  <p>

  <a href="{{ route('listing.create') }}">
    <button type="button" class="btn btn-outline-primary">Add New Listing</button>  
    </a> 
    </p>      
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Name</th>
        <th>Address</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Action</th>
        

</div>
      </tr>
    </thead>
    <tbody>
    
    @forelse($listings as $listing)

      <tr>
        <td>{{ $listing->list_name }}</td>
        <td>{{ $listing->address }}</td>
        <td>{{ $listing->latitude }}</td>
        <td>{{ $listing->longitude }}</td>
        
        <td> 
        <div class="btn-group" role="group" aria-label="Basic example">
        
                <a href="{{ route('listing.show', [$listing->id]) }}">
                    <button type="button" class="btn btn-success btn-sm">View</button>
               </a>

               <a href="{{ route('listing.edit', [$listing->id]) }}">
                    <button type="button" class="btn btn-warning btn-sm">Edit</button>
              </a>

              
              
              <form action="{{ route('listing.destroy', $listing->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger btn-sm" type="submit" onclick="confirm('Are you sure to delete this data ?')">Delete</button>
                                    </form>
                                 
</div>
              

              </td>
        

      </tr>
                             @empty
                            <tr>
                                <td colspan="5">No Record Found</td>
                            </tr>
                            @endforelse

    </tbody>
  </table>

  <div style="float:left; margin-right: 5px;">
                        <div class="pagination">
                            <h5 class="page-item"> Showing {{$listings->total()}} - {{$listings->total()}} of {{$listings->total()}}</h5>
                        </div>
                    </div>
                    <div style="float:right; margin-right: 5px;">
                        {!! $listings->appends(\Request::except('page'))->render("pagination::bootstrap-4") !!} 
                    </div>
                    
</div>
@endsection