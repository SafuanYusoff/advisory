@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>View User
                        <a href="{{ route('user.index') }}">
                            <button class="btn btn-success pull-right" style="float: right;">Return</button>
                        </a>
                    </h3>
                </div>

                <div class="card-body">
               
                     <div class="form-group col-md-6">
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $user->name }}" class="form-control" >
                    </div>

                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input type="text" name="email" value="{{ $user->email }}" class="form-control" >
                    </div>

                    <div class="form-group col-md-6">
                        <label>Created At</label>
                        <input type="text" name="created_at" value="{{ $user->created_at }}" class="form-control">
                    </div>
                   

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
