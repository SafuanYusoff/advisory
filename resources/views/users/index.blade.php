@extends('layouts.app')

@section('content')
<div class="container">
  <h2>List of User</h2>
  <p>

  <a href="{{ route('user.create') }}">
    <button type="button" class="btn btn-outline-primary">Add New User</button>  
    </a> 
    </p>      
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Created At</th>
        <th>Action</th>
        

</div>
      </tr>
    </thead>
    <tbody>
    
    @forelse($users as $user)

      <tr>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->created_at }}</td>

        
        <td> 
        <div class="btn-group" role="group" aria-label="Basic example">
        
                <a href="{{ route('user.show', [$user->id]) }}">
                    <button type="button" class="btn btn-success btn-sm">View</button>
               </a>

               <a href="{{ route('user.edit', [$user->id]) }}">
                    <button type="button" class="btn btn-warning btn-sm">Edit</button>
              </a>

              

            @if($user->type != 'a')
            <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn btn-danger btn-sm" type="submit" onclick="confirm('Are you sure to delete this data ?')">Delete</button>
            </form>
            @endif
                                 
      </div>
              

              </td>
        

          </tr>
                             @empty
                            <tr>
                                <td colspan="5">No Record Found</td>
                            </tr>
                            @endforelse

    </tbody>
  </table>

  <div style="float:left; margin-right: 5px;">
                        <div class="pagination">
                            <h5 class="page-item"> Showing {{$users->total()}} - {{$users->total()}} of {{$users->total()}}</h5>
                        </div>
                    </div>
                    <div style="float:right; margin-right: 5px;">
                        {!! $users->appends(\Request::except('page'))->render("pagination::bootstrap-4") !!} 
                    </div>
                    
</div>
@endsection