<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return collect([
            ['name' => 'admin', 'email' => 'admin@advisory.com', 'type' => 'a'],
            ['name' => 'user', 'email' => 'user@advisory.com', 'type' => 'u'],
        ])
        ->each(function ($user) {

            $registeredUser = User::create([
                'name'      => $user['name'],
                'email'     => $user['email'],
                'password'  => bcrypt('password'),
                'type'  	=> $user['type'],
            ]);

        });

    }
}
